/* -*-  Mode: C++; c-file-style: "gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 The Boeing Company
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

// This script configures two nodes on an 802.11b physical layer, with
// 802.11b NICs in infrastructure mode, and by default, the station sends
// one packet of 1000 (application) bytes to the access point.  The
// physical layer is configured
// to receive at a fixed RSS (regardless of the distance and transmit
// power); therefore, changing position of the nodes has no effect.
//
// There are a number of command-line options available to control
// the default behavior.  The list of available command-line options
// can be listed with the following command:
// ./waf --run "wifi-simple-infra --help"
//
// For instance, for this configuration, the physical layer will
// stop successfully receiving packets when rss drops below -97 dBm.
// To see this effect, try running:
//
// ./waf --run "wifi-simple-infra --rss=-97 --numPackets=20"
// ./waf --run "wifi-simple-infra --rss=-98 --numPackets=20"
// ./waf --run "wifi-simple-infra --rss=-99 --numPackets=20"
//
// Note that all ns-3 attributes (not just the ones exposed in the below
// script) can be changed at command line; see the documentation.
//
// This script can also be helpful to put the Wifi layer into verbose
// logging mode; this command will turn on all wifi logging:
//
// ./waf --run "wifi-simple-infra --verbose=1"
//
// When you are done, you will notice two pcap trace files in your directory.
// If you have tcpdump installed, you can try this:
//
// tcpdump -r wifi-simple-infra-0-0.pcap -nn -tt
//

#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/double.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/ssid.h"
#include "ns3/mobility-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/mobility-model.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/flow-monitor-module.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("WifiSimpleInfra");

void ReceivePacket (Ptr<Socket> socket)
{
  while (socket->Recv ())
    {
      NS_LOG_UNCOND ("Received one packet!");
    }
}

static void GenerateTraffic (Ptr<Socket> socket, uint32_t pktSize,
                             uint32_t pktCount, Time pktInterval )
{
  /*FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();*/
  //monitor = flowmon.Install(c_ap);
  //monitor = flowmon.Install(c_sta);
  if (pktCount > 0)
    {
      socket->Send (Create<Packet> (pktSize));
      Simulator::Schedule (pktInterval, &GenerateTraffic,
                           socket, pktSize,pktCount - 1, pktInterval);
    }
  else
    {
      socket->Close ();
    }
}

/*static void GenerateTraffic (Ptr<Socket> socket, uint32_t pktSize,
                             uint32_t pktCount, Time pktInterval, FlowMonitorHelper flowmon, Ptr<FlowMonitor> monitor)
{
  if (pktCount > 0)
    {
      socket->Send (Create<Packet> (pktSize));
      monitor->CheckForLostPackets ();
	  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
	  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();

	  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
	  {
		Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);
		NS_LOG_UNCOND("Flow ID: " << iter->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress);
		NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
		NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
		NS_LOG_UNCOND("Throughput: " << iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds()) / 1024  << " Kbps");
	  }
	  monitor->SerializeToXmlFile ("results.xml" , true, true );
      Simulator::Schedule (pktInterval, &GenerateTraffic,
                           socket, pktSize,pktCount - 1, pktInterval, flowmon, monitor);
    }
  else
    {
      socket->Close ();
    }
}*/

int main (int argc, char *argv[])
{

  LogComponentEnable ("WifiSimpleInfra", LOG_LEVEL_INFO);
  LogComponentEnable ("Socket", LOG_LEVEL_INFO);
  LogComponentEnable ("Ipv4AddressHelper", LOG_LEVEL_INFO);
  //LogComponentEnable ("WifiPhy", LOG_LEVEL_INFO);

  std::string phyMode ("DsssRate1Mbps");
  double rss = -80;  // -dBm
  uint32_t packetSize = 1000; // bytes
  uint32_t numPackets = 150;
  double interval = 1.0; // seconds
  bool verbose = false;
  //ns3 and mininet connect using opnet

  CommandLine cmd;
  cmd.AddValue ("phyMode", "Wifi Phy mode", phyMode);
  cmd.AddValue ("rss", "received signal strength", rss);
  cmd.AddValue ("packetSize", "size of application packet sent", packetSize);
  cmd.AddValue ("numPackets", "number of packets generated", numPackets);
  cmd.AddValue ("interval", "interval (seconds) between packets", interval);
  cmd.AddValue ("verbose", "turn on all WifiNetDevice log components", verbose);
  cmd.Parse (argc, argv);
  // Convert to time object
  Time interPacketInterval = Seconds (interval);

  // Fix non-unicast data rate to be the same as that of unicast
  Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",
                      StringValue (phyMode));

  //STA(0) AP(1) STA(2) AP(3) STA(4) AP(5) STA(6)

  NodeContainer c;
  c.Create (7);

  NodeContainer c_ap;
  NodeContainer c_sta;
  c_sta.Add (c.Get (0));
  c_ap.Add (c.Get (1));
  c_sta.Add (c.Get (2));
  c_ap.Add (c.Get (3));
  c_sta.Add (c.Get (4));
  c_ap.Add (c.Get (5));
  c_sta.Add (c.Get (6));

  // The below set of helpers will help us to put together the wifi NICs we want
  WifiHelper wifi;
  if (verbose)
    {
      wifi.EnableLogComponents ();  // Turn on all Wifi logging
    }
  wifi.SetStandard (WIFI_PHY_STANDARD_80211b);

  YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
  // This is one parameter that matters when using FixedRssLossModel
  // set it to zero; otherwise, gain will be added
  wifiPhy.Set ("RxGain", DoubleValue (0) );
  // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
  wifiPhy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
  //wifiPhy.Set("ChannelNumber",UintegerValue(channelNumber1));

  YansWifiPhyHelper wifiPhy2 =  YansWifiPhyHelper::Default ();
  // This is one parameter that matters when using FixedRssLossModel
  // set it to zero; otherwise, gain will be added
  wifiPhy2.Set ("RxGain", DoubleValue (0) );
  // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
  wifiPhy2.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
  //wifiPhy2.Set("ChannelNumber",UintegerValue(channelNumber2));

  YansWifiPhyHelper wifiPhy3 =  YansWifiPhyHelper::Default ();
  // This is one parameter that matters when using FixedRssLossModel
  // set it to zero; otherwise, gain will be added
  wifiPhy3.Set ("RxGain", DoubleValue (0) );
  // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
  wifiPhy3.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);
  //wifiPhy3.Set("ChannelNumber",UintegerValue(channelNumber3));

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  // The below FixedRssLossModel will cause the rss to be fixed regardless
  // of the distance between the two stations, and the transmit power
  wifiChannel.AddPropagationLoss ("ns3::FixedRssLossModel","Rss",DoubleValue (rss));
  wifiPhy.SetChannel (wifiChannel.Create ());
  wifiPhy2.SetChannel (wifiChannel.Create ());
  wifiPhy3.SetChannel (wifiChannel.Create ());

  // Add a mac and disable rate control
  WifiMacHelper wifiMac;
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode",StringValue (phyMode),
                                "ControlMode",StringValue (phyMode));

  // Setup the rest of the mac
  Ssid ssid = Ssid ("wifi-default");
  // setup sta.
  //STA(0) AP(1) STA(2) AP(3) STA(4) AP(5) STA(6)
  wifiMac.SetType ("ns3::StaWifiMac",
                   "Ssid", SsidValue (ssid));
  NetDeviceContainer staDevice1 = wifi.Install (wifiPhy, wifiMac, c.Get (0));	//STA
  NetDeviceContainer staDevice2 = wifi.Install (wifiPhy2, wifiMac, c.Get (6));	//STA
  NetDeviceContainer staDevice3 = wifi.Install (wifiPhy3, wifiMac, c.Get (2));	//STA
  NetDeviceContainer staDevice4 = wifi.Install (wifiPhy2, wifiMac, c.Get (4));	//STA
  NetDeviceContainer devices1 = staDevice1;
  NetDeviceContainer devices2 = staDevice2;
  NetDeviceContainer devices3 = staDevice3;
  devices3.Add (staDevice4);
  // setup ap.
  wifiMac.SetType ("ns3::ApWifiMac",
                   "Ssid", SsidValue (ssid));
  NetDeviceContainer apDevice1 = wifi.Install (wifiPhy, wifiMac, c.Get (1));		//AP
  NetDeviceContainer apDevice2 = wifi.Install (wifiPhy2, wifiMac, c.Get (5));		//AP
  NetDeviceContainer apDevice3 = wifi.Install (wifiPhy3, wifiMac, c.Get (3));		//AP
  devices1.Add (apDevice1);
  devices2.Add (apDevice2);
  devices3.Add (apDevice3);

  // Note that with FixedRssLossModel, the positions below are not
  // used for received signal strength.
  MobilityHelper mobility;
  Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator> ();
  positionAlloc->Add (Vector (0.0, 0.0, 0.0));
  positionAlloc->Add (Vector (2.0, 0.0, 0.0));
  positionAlloc->Add (Vector (4.0, 0.0, 0.0));
  positionAlloc->Add (Vector (6.0, 0.0, 0.0));
  positionAlloc->Add (Vector (8.0, 0.0, 0.0));
  positionAlloc->Add (Vector (10.0, 0.0, 0.0));
  positionAlloc->Add (Vector (12.0, 0.0, 0.0));
  mobility.SetPositionAllocator (positionAlloc);
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (c);

  InternetStackHelper internet;
  internet.Install (c);



  //STA(0) AP(1) STA(2) AP(3) STA(4) AP(5) STA(6)
  Ipv4AddressHelper ipv4_1;
  NS_LOG_INFO ("Assign IP Addresses.");
  ipv4_1.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i1 = ipv4_1.Assign (devices1);



  TypeId tid1 = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink1 = Socket::CreateSocket (c.Get (0), tid1);				//STA is receiver
  InetSocketAddress local1 = InetSocketAddress (Ipv4Address::GetAny (), 80);
  NS_LOG_DEBUG(local1<<" is the STA\n");
  recvSink1->Bind (local1);
  recvSink1->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source1 = Socket::CreateSocket (c.Get (1), tid1);					//AP is sender
  InetSocketAddress remote1 = InetSocketAddress (Ipv4Address ("10.1.1.25"), 80);
  NS_LOG_DEBUG(remote1<<" is the AP\n");
  source1->SetAllowBroadcast (true);
  source1->Connect (remote1);

  TypeId tid1_1 = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink1_1 = Socket::CreateSocket (c.Get (1), tid1_1);				//AP is receiver
  InetSocketAddress local1_1 = InetSocketAddress (Ipv4Address::GetAny (), 81);
  recvSink1_1->Bind (local1_1);
  recvSink1_1->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source1_1 = Socket::CreateSocket (c.Get (0), tid1_1);					//STA is sender
  InetSocketAddress remote1_1 = InetSocketAddress (Ipv4Address ("255.255.255.255"), 81);
  source1_1->SetAllowBroadcast (true);
  source1_1->Connect (remote1_1);





  //STA(0) AP(1) STA(2) AP(3) STA(4) AP(5) STA(6)
  Ipv4AddressHelper ipv4_2;
  NS_LOG_INFO ("Assign IP Addresses.");
  ipv4_2.SetBase ("11.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i2 = ipv4_2.Assign (devices2);

  TypeId tid2 = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink2 = Socket::CreateSocket (c.Get (6), tid2);		//STA is receiver
  InetSocketAddress local2 = InetSocketAddress (Ipv4Address::GetAny (), 90);
  recvSink2->Bind (local2);
  recvSink2->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source2 = Socket::CreateSocket (c.Get (5), tid2);			//AP is sender
  InetSocketAddress remote2 = InetSocketAddress (Ipv4Address ("255.255.255.255"), 90);
  source2->SetAllowBroadcast (true);
  source2->Connect (remote2);

  TypeId tid2_1 = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink2_1 = Socket::CreateSocket (c.Get (5), tid2_1);		//AP is receiver
  InetSocketAddress local2_1 = InetSocketAddress (Ipv4Address::GetAny (), 91);
  recvSink2_1->Bind (local2_1);
  recvSink2_1->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source2_1 = Socket::CreateSocket (c.Get (6), tid2_1);			//STA is sender
  InetSocketAddress remote2_1 = InetSocketAddress (Ipv4Address ("255.255.255.255"), 91);
  source2_1->SetAllowBroadcast (true);
  source2_1->Connect (remote2_1);





  //STA(0) AP(1) STA(2) AP(3) STA(4) AP(5) STA(6)
  Ipv4AddressHelper ipv4_3;
  NS_LOG_INFO ("Assign IP Addresses.");
  ipv4_3.SetBase ("12.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i3 = ipv4_3.Assign (devices3);

  TypeId tid3 = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink3 = Socket::CreateSocket (c.Get (2), tid3);		//STA is receiver
  InetSocketAddress local3 = InetSocketAddress (Ipv4Address::GetAny (), 100);
  recvSink3->Bind (local3);
  recvSink3->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source3 = Socket::CreateSocket (c.Get (3), tid3);			//AP is sender
  InetSocketAddress remote3 = InetSocketAddress (Ipv4Address ("10.0.0.1"), 100);
  source3->SetAllowBroadcast (true);
  source3->Connect (remote3);

  TypeId tid3_1 = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink3_1 = Socket::CreateSocket (c.Get (3), tid3_1);		//AP is receiver
  InetSocketAddress local3_1 = InetSocketAddress (Ipv4Address::GetAny (), 101);
  recvSink3_1->Bind (local3_1);
  recvSink3_1->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source3_1 = Socket::CreateSocket (c.Get (2), tid3_1);			//STA is sender
  InetSocketAddress remote3_1 = InetSocketAddress (Ipv4Address ("255.255.255.255"), 101);
  source3_1->SetAllowBroadcast (true);
  source3_1->Connect (remote3_1);



  TypeId tid32 = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink32 = Socket::CreateSocket (c.Get (4), tid32);		//STA is receiver
  InetSocketAddress local32 = InetSocketAddress (Ipv4Address::GetAny (), 110);
  recvSink32->Bind (local32);
  recvSink32->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source32 = Socket::CreateSocket (c.Get (3), tid32);			//AP is sender
  InetSocketAddress remote32 = InetSocketAddress (Ipv4Address ("255.255.255.255"), 110);
  source32->SetAllowBroadcast (true);
  source32->Connect (remote32);

  TypeId tid32_1 = TypeId::LookupByName ("ns3::UdpSocketFactory");
  Ptr<Socket> recvSink32_1 = Socket::CreateSocket (c.Get (3), tid32_1);		//AP is receiver
  InetSocketAddress local32_1 = InetSocketAddress (Ipv4Address::GetAny (), 111);
  recvSink32_1->Bind (local32_1);
  recvSink32_1->SetRecvCallback (MakeCallback (&ReceivePacket));

  Ptr<Socket> source32_1 = Socket::CreateSocket (c.Get (4), tid32_1);			//STA is sender
  InetSocketAddress remote32_1 = InetSocketAddress (Ipv4Address ("255.255.255.255"), 111);
  source32_1->SetAllowBroadcast (true);
  source32_1->Connect (remote32_1);



  // Tracing
  //wifiPhy.EnablePcap ("wifi-simple-infra1", devices1);
  //wifiPhy.EnablePcap ("wifi-simple-infra2", devices2);

  // Output what we are doing
  NS_LOG_UNCOND ("Testing " << numPackets  << " packets sent with receiver rss " << rss );


  FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();
  //monitor = flowmon.Install(c_ap);
  //monitor = flowmon.Install(c_sta);
  NS_LOG_DEBUG(source1->GetNode ()->GetId ()<<" is the context\n");
  Simulator::ScheduleWithContext (source1->GetNode ()->GetId (),
                                  Seconds (1.0), &GenerateTraffic,
                                  source1, packetSize, numPackets, interPacketInterval);

  /*Simulator::ScheduleWithContext (source1_1->GetNode ()->GetId (),
                                  Seconds (1.1), &GenerateTraffic,
                                  source1_1, packetSize, numPackets, interPacketInterval);

  Simulator::ScheduleWithContext (source2->GetNode ()->GetId (),
                                  Seconds (1.5), &GenerateTraffic,
                                  source2, packetSize, numPackets, interPacketInterval);

  Simulator::ScheduleWithContext (source2_1->GetNode ()->GetId (),
                                  Seconds (1.6), &GenerateTraffic,
                                  source2_1, packetSize, numPackets, interPacketInterval);

  Simulator::ScheduleWithContext (source3->GetNode ()->GetId (),
                                  Seconds (2.0), &GenerateTraffic,
                                  source3, packetSize, numPackets, interPacketInterval);

  Simulator::ScheduleWithContext (source3_1->GetNode ()->GetId (),
                                  Seconds (2.1), &GenerateTraffic,
                                  source3_1, packetSize, numPackets, interPacketInterval);

  Simulator::ScheduleWithContext (source32->GetNode ()->GetId (),
                                  Seconds (2.5), &GenerateTraffic,
                                  source32, packetSize, numPackets, interPacketInterval);

  Simulator::ScheduleWithContext (source32_1->GetNode ()->GetId (),
                                  Seconds (2.6), &GenerateTraffic,
                                  source32_1, packetSize, numPackets, interPacketInterval);*/

  /*FlowMonitorHelper flowmon;
  Ptr<FlowMonitor> monitor = flowmon.InstallAll();*/
  //monitor = flowmon.Install(c_ap);
//  monitor = flowmon.Install(c_sta);


  Simulator::Stop (Seconds (10.0));
  Simulator::Run ();

  monitor->SerializeToXmlFile("testmahesh.xml", true, true);

  monitor->CheckForLostPackets ();
  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier> (flowmon.GetClassifier ());
  std::map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
  // Print per flow statistics
  for (std::map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
  {
    printf("HELLO");
    Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);

 	  NS_LOG_UNCOND("Flow ID: " << iter->first << " Src Addr " << t.sourceAddress << " Dst Addr " << t.destinationAddress);
    NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
    NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
    NS_LOG_UNCOND("Throughput: " << iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds()) / 1024  << " Kbps");
  }

  Simulator::Destroy ();

  return 0;
}
