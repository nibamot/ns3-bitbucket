#!/usr/bin/env python3
import csv
import matplotlib.pyplot as pyplot

# MCS 0 6.5
a = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
bstring = []
b=[]
abr = 5
with open('Output-Training-A1payload1470MCS0.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            bstring.append(row[16])

csvfile.close()
bnum = list (map(float, bstring))
b = [(1-(x/100))*abr for x in bnum]



# MCS 1 13
c = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
dstring = []
d=[]
cbr = 9
with open('Output-Training-A1payload1470MCS1.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            dstring.append(row[16])

csvfile.close()
dnum = list (map(float, dstring))
d = [(1-(x/100))*cbr for x in dnum]



# MCS 2
e = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
fstring = []
f=[]
ebr = 16.6
with open('Output-Training-A1payload1470MCS2.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            fstring.append(row[16])

csvfile.close()
fnum = list (map(float, fstring))
f = [(1-(x/100))*ebr for x in fnum]



# MCS 3
g = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
hstring = []
h=[]
gbr = 22
with open('Output-Training-A1payload1470MCS3.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            hstring.append(row[16])

csvfile.close()
hnum = list (map(float, hstring))
h = [(1-(x/100))*gbr for x in hnum]

# MCS 4
i1 = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
jstring = []
j=[]
i1br = 31.4
with open('Output-Training-A1payload1470MCS4.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            jstring.append(row[16])

csvfile.close()
jnum = list (map(float, jstring))
j = [(1-(x/100))*i1br for x in jnum]


# MCS 5
k = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
lstring = []
l=[]
kbr = 41
with open('Output-Training-A1payload1470MCS5.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            lstring.append(row[16])

csvfile.close()
lnum = list (map(float, lstring))
l = [(1-(x/100))*kbr for x in lnum]


# MCS 6
m = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
nstring = []
n=[]
mbr = 42
with open('Output-Training-A1payload1470MCS6.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            nstring.append(row[16])

csvfile.close()
nnum = list (map(float, nstring))
n = [(1-(x/100))*mbr for x in nnum]



# MCS 7
o = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
pstring = []
p=[]
obr = 43.8
with open('Output-Training-A1payload1470MCS7.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            pstring.append(row[16])

csvfile.close()
pnum = list (map(float, pstring))
p = [(1-(x/100))*obr for x in pnum]




# MCS 8
q = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
rstring = []
r=[]
qbr = 11.3
with open('Output-Training-A1payload1470MCS8.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            rstring.append(row[16])

csvfile.close()
rnum = list (map(float, rstring))
r = [(1-(x/100))*qbr for x in rnum]



# MCS 9
s = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
tstring = []
t=[]
sbr = 22
with open('Output-Training-A1payload1470MCS9.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            tstring.append(row[16])

csvfile.close()
tnum = list (map(float, tstring))
t = [(1-(x/100))*sbr for x in tnum]



# MCS 10
u = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
vstring = []
v=[]
ubr = 31.4
with open('Output-Training-A1payload1470MCS10.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            vstring.append(row[16])

csvfile.close()
vnum = list (map(float, vstring))
v = [(1-(x/100))*ubr for x in vnum]




# MCS 11
w = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
xstring = []
x1=[]
wbr = 41
with open('Output-Training-A1payload1470MCS11.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            xstring.append(row[16])

csvfile.close()
xnum = list (map(float, xstring))
x1 = [(1-(x/100))*wbr for x in xnum]



# MCS 12
y = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
zstring = []
z=[]
ybr = 56.8
with open('Output-Training-A1payload1470MCS12.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            zstring.append(row[16])

csvfile.close()
znum = list (map(float, zstring))
z = [(1-(x/100))*ybr for x in znum]


# MCS 13
a1 = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
a2string = []
a2=[]
a1br = 70
with open('Output-Training-A1payload1470MCS13.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            a2string.append(row[16])

csvfile.close()
a2num = list (map(float, a2string))
a2 = [(1-(x/100))*a1br for x in a2num]




# MCS 14
b1 = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
b2string = []
b2=[]
b1br = 76.8
with open('Output-Training-A1payload1470MCS14.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            b2string.append(row[16])

csvfile.close()
b2num = list (map(float, b2string))
b2 = [(1-(x/100))*b1br for x in b2num]



# MCS 15
c1 = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
c2string = []
c2=[]
c1br = 84
with open('Output-Training-A1payload1470MCS15.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            c2string.append(row[16])

csvfile.close()
c2num = list (map(float, c2string))
c2 = [(1-(x/100))*c1br for x in c2num]




o1=[160]
o2=[2]

# matplotlib plot
pyplot.plot(o1,o2)
pyplot.plot(a,b,label='MCS 0, 5.76Mbps')
pyplot.plot(c,d,label='MCS 1, 11Mbps')
pyplot.plot(e,f,label='MCS 2, 16Mbps')
pyplot.plot(g,h,label='MCS 3, 22Mbps')
pyplot.plot(i1,j,label='MCS 4, 31.4Mbps')
pyplot.plot(k,l,label='MCS 5, 41Mbps')
pyplot.plot(m,n,label='MCS 6, 42Mbps')
pyplot.plot(o,p,label='MCS 7, 43.8Mbps')
pyplot.plot(q,r,label='MCS 8, 11.3Mbps')
pyplot.plot(s,t,label='MCS 9, 22Mbps')
pyplot.plot(u,v,label='MCS 10, 31.4Mbps')
pyplot.plot(w,x1,label='MCS 11, 41Mbps')
pyplot.plot(y,z,label='MCS 12, 56.8Mbps')
pyplot.plot(a1,a2,label='MCS 13, 70Mbps')
pyplot.plot(b1,b2,label='MCS 14, 76.8Mbps')
pyplot.plot(c1,c2,label='MCS 15, 84Mbps')
pyplot.xlabel('Distance from AP at (0,0)')
pyplot.ylabel('Estimated Bitrate (Mbps)')
pyplot.title('Distance in metres vs Estimated Bitrate for  Payload 1470, 1 Station  ')
pyplot.legend()

pyplot.show()
