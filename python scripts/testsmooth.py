#!/usr/bin/env python3
import csv
import matplotlib.pyplot as pyplot
import seaborn as sns
import numpy as np
from scipy.interpolate import interp1d
from matplotlib.backends.backend_pdf import PdfPages

NUM_COLORS = 10
LINE_STYLES = ['solid', 'dashed', 'dashdot', 'dotted']
NUM_STYLES = len(LINE_STYLES)

sns.reset_orig()
clrs = sns.color_palette('husl', n_colors=NUM_COLORS)
# MCS 0 1000
a = [80, 82.5, 85, 87.5, 90, 92.5, 95, 97.5, 100, 102.5, 105, 107.5, 110, 112.5, 115, 117.5, 120]
bnum = [0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 3.13067, 71.0507, 98.4107, 100, 100, 100, 100, 100, 100, 100]
b = [((100-x)) for x in bnum]

a_smooth = np.array(a)
b_smooth = np.array(b)

aa_smooth = np.linspace(a_smooth.min(), a_smooth.max(), 500)
spl = interp1d(a_smooth, b_smooth, kind='linear', fill_value='extrapolate')
#print(aa_smooth)
bb_smooth = spl(aa_smooth)
#print(bb_smooth)


o1=[75]
o2=[0]

# matplotlib plot
with PdfPages('DPvsDist.pdf') as pdf:
    pyplot.plot(o1,o2)
    pyplot.plot(aa_smooth,bb_smooth,label='Frame length = 1000 bytes', color=clrs[0], linestyle=LINE_STYLES[0])
    pyplot.scatter(a,b)

    pyplot.xlabel('Distance from AP [m]')
    pyplot.ylabel('Delivery Probability [%]')
#pyplot.title('Distance in metres vs Probability of delivery for  MCS 0, 3Mbps single station')
    pyplot.legend()
    #fig = pyplot.figure(figsize=(10,10))
    pdf.savefig()
    pyplot.show()
#pdf.close()
