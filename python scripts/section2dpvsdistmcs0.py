#!/usr/bin/env python3
import csv
import matplotlib.pyplot as pyplot
import seaborn as sns
import numpy as np
from scipy.interpolate import interp1d
from matplotlib.backends.backend_pdf import PdfPages

NUM_COLORS = 10
LINE_STYLES = ['solid', 'dashed', 'dashdot', 'dotted']
NUM_STYLES = len(LINE_STYLES)

sns.reset_orig()
clrs = sns.color_palette('husl', n_colors=NUM_COLORS)
# MCS 0 1000
a = [80, 82.5, 85, 87.5, 90, 92.5, 95, 97.5, 100, 102.5, 105, 107.5, 110, 112.5, 115, 117.5, 120]
bnum = [0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 3.13067, 71.0507, 98.4107, 100, 100, 100, 100, 100, 100, 100]
b = [((100-x)) for x in bnum]
a_smooth = np.array(a)
b_smooth = np.array(b)
aa_smooth = np.linspace(a_smooth.min(), a_smooth.max(), 500)
spl = interp1d(a_smooth, b_smooth, kind='linear')
bb_smooth = spl(aa_smooth)



# MCS 1 1500
c = [80, 82.5, 85, 87.5, 90, 92.5, 95, 97.5, 100, 102.5, 105, 107.5, 110, 112.5, 115, 117.5, 120]
dnum = [0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 17.8987, 83.824, 99.776, 100, 100, 100, 100, 100, 100, 100]
d = [((100-x)) for x in dnum]
c_smooth = np.array(c)
d_smooth = np.array(d)
cc_smooth = np.linspace(c_smooth.min(), c_smooth.max(), 500)
spl = interp1d(c_smooth, d_smooth, kind='linear')
dd_smooth = spl(cc_smooth)

# MCS 2000
e = [80, 82.5, 85, 87.5, 90, 92.5, 95, 97.5, 100, 102.5, 105, 107.5, 110, 112.5, 115, 117.5, 120]
fnum = [0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 32.5867, 91.2107, 99.936, 100, 100, 100, 100, 100, 100, 100]
f = [((100-x)) for x in fnum]
e_smooth = np.array(e)
f_smooth = np.array(f)
ee_smooth = np.linspace(e_smooth.min(), e_smooth.max(), 500)
spl = interp1d(e_smooth, f_smooth, kind='linear')
ff_smooth = spl(ee_smooth)


# MCS 2500
g = [80, 82.5, 85, 87.5, 90, 92.5, 95, 97.5, 100, 102.5, 105, 107.5, 110, 112.5, 115, 117.5, 120]
hnum = [0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 45.6853, 94.9333, 99.9733, 100, 100, 100, 100, 100, 100, 100]
h = [((100-x)) for x in hnum]
g_smooth = np.array(g)
h_smooth = np.array(h)
gg_smooth = np.linspace(g_smooth.min(), g_smooth.max(), 500)
spl = interp1d(g_smooth, h_smooth, kind='linear')
hh_smooth = spl(gg_smooth)

# MCS 3000
i1 = [80, 82.5, 85, 87.5, 90, 92.5, 95, 97.5, 100, 102.5, 105, 107.5, 110, 112.5, 115, 117.5, 120]
jnum = [0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 54.4693, 97.216, 100, 100, 100, 100,  100, 100, 100, 100]
j = [((100-x)) for x in jnum]
i1_smooth = np.array(i1)
j_smooth = np.array(j)
i1i_smooth = np.linspace(i1_smooth.min(), i1_smooth.max(), 500)
spl = interp1d(i1_smooth, j_smooth, kind='linear')
jj_smooth = spl(i1i_smooth)


# MCS 3500
k = [80, 82.5, 85, 87.5, 90, 92.5, 95, 97.5, 100, 102.5, 105, 107.5, 110, 112.5, 115, 117.5, 120]
lnum = [0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 0.032, 62.0587, 98.3573, 100, 100, 100, 100,  100, 100, 100, 100]
l = [((100-x)) for x in lnum]
k_smooth = np.array(k)
l_smooth = np.array(l)
kk_smooth = np.linspace(k_smooth.min(), k_smooth.max(), 500)
spl = interp1d(k_smooth, l_smooth, kind='linear')
ll_smooth = spl(kk_smooth)

pyplot.rcParams.update({'font.size': 30})
o1=[79]
o2=[0]

# matplotlib plot
pyplot.plot(o1,o2)
pyplot.plot(aa_smooth,bb_smooth,label='Frame length = 1000 bytes', color='b', linestyle=LINE_STYLES[0], linewidth=2.5)
pyplot.plot(cc_smooth,dd_smooth,label='Frame length = 1500 bytes', color='b', linestyle=LINE_STYLES[1], linewidth=2.5)
pyplot.plot(ee_smooth,ff_smooth,label='Frame length = 2000 bytes', color='b', linestyle=LINE_STYLES[2], linewidth=2.5)
pyplot.plot(gg_smooth,hh_smooth,label='Frame length = 2500 bytes', color='b', linestyle=LINE_STYLES[3], linewidth=2.5)
pyplot.plot(i1i_smooth,jj_smooth,label='Frame length = 3000 bytes', color='b', linestyle=(0, (1, 10)), linewidth=2.5)
pyplot.plot(kk_smooth,ll_smooth,label='Frame length = 3500 bytes', color='b', linestyle=(0, (3, 1, 1, 1, 1, 1)), linewidth=2.5)
#pyplot.gca().spines['bottom'].set_position(('data',-0))
#pyplot.gca().spines['bottom'].set_smart_bounds(True)
#pyplot.gca().spines['left'].set_position(('data',50))
#pyplot.gca().spines['left'].set_smart_bounds(True)
#pyplot.gca().spines['top'].set_smart_bounds(True)
#pyplot.gca().spines['right'].set_position(('data',125))
#pyplot.gca().spines['top'].set_smart_bounds(True)
#pyplot.gca().spines['right'].set_smart_bounds(True)
pyplot.yticks([0, 20, 40, 60, 80, 100])
pyplot.xticks([80, 90, 100, 110, 120])
pyplot.xlabel('Distance from AP [m]')
pyplot.ylabel('Delivery Probability [%]')
#pyplot.title('Distance in metres vs Probability of delivery for  MCS 0, 3Mbps single station')
pyplot.legend()#loc = 'upper left', bbox_to_anchor=(0.59, 1.0)
#fig = pyplot.figure(figsize=(10,10))
#pyplot.savefig( 'test.pdf',bbox_inches='tight', transparent="True", pad_inches=0)
pyplot.show()
#pdf.close()
