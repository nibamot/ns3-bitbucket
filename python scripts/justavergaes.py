#!/usr/bin/env python3
#after 420 lines change aggregation size then mcs

import csv


firstrow = ['Throughput', 'Goodput', 'Packet Loss ratio', 'Channe Utilization', 'Bitrate', 'MCS', 'Aggregation size']
rowavg = ['AVERAGE']
rowmedian = ['MEDIAN']
rowci = ['C.I']
rowstdev = ['STD DEVIATION']

avgform = ['=AVERAGE(A2:A21)', '=AVERAGE(B2:B21 )', '=AVERAGE(C2:C21 )', '=AVERAGE(D2:D21 )']
medianform = ['=MEDIAN(A2:A21)', '=MEDIAN(B2:B21 )', '=MEDIAN(C2:C21 )', '=MEDIAN(D2:D21 )']
ciform = ['=CONFIDENCE(0.05, STDEV(A2:A21),20)', '=CONFIDENCE(0.05, STDEV(B2:B21),20)', '=CONFIDENCE(0.05, STDEV(C2:C21),20)', '=CONFIDENCE(0.05, STDEV(D2:D21),20)' ]
stdevform = ['=STDEV(A2:A21)', '=STDEV(B2:B21 )', '=STDEV(C2:C21 )', '=STDEV(D2:D21)']


z=['']
tp = []
gp = []
avgrow = [0]*6
dict = {'5120':'1','5125':'2','5129':'3','51212':'4','51215':'5','10240':'6','10245':'7',
        '10249':'8','102412':'9','102415':'10','20480':'11','20485':'12','20489':'13','204812':'14',
        '204815':'15','38390':'16','38395':'17','38399':'18','383912':'19','383915':'20'}
line = 21




p = 0
with open('noaggregation200.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    with open('noaggregationavg200.csv', 'w') as newcsv:
        csvwriter = csv.writer(newcsv)
        for i, row in enumerate(csvreader):

                if 'Average' in row:
                    avgrow[0] = row[11]
                    avgrow[1] = row[12]
                    avgrow[2] = row[15]
                    avgrow[3] = row[17]
                    avgrow[4] = row[10]
                    #avgrow[5] = dict[str(int(float(row[8])))+str(int(float(row[9])))]
                    #print(avgrow)
                    csvwriter.writerow(avgrow)
