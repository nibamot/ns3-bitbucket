#!/usr/bin/env python3
#after 420 lines change aggregation size then mcs

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import csv
from matplotlib import cm
import matplotlib
font = {'family' : 'normal',
        'size'   : 18}

matplotlib.rc('font', **font)

fig = plt.figure()

noaggstation1 = []
noaggstation2 = []
noaggstation3 = []
noaggstation4 = []
noaggstation5 = []
noaggtotal = []

maxaggstation1 = []
maxaggstation2 = []
maxaggstation3 = []
maxaggstation4 = []
maxaggstation5 = []
maxaggtotal = []

rfraggstation1 = []
rfraggstation2 = []
rfraggstation3 = []
rfraggstation4 = []
rfraggstation5 = []
rfraggtotal = []

#y = np.linspace(133)
axes= plt.axes()
axes.grid()
axes.set_axisbelow(True)
with open('Goodputaeach1stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggstation1.append(float(row[0]))
csvfile.close()

with open('Goodputaeach1stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggstation1.append(float(row[0]))
csvfile.close()

with open('Goodputaeach1stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggstation1.append(float(row[0]))
csvfile.close()
x  = np.linspace(0, 10.1, 100)

noagg = plt.plot(x ,noaggstation1, label = 'No Aggregation', color = '#1e3f63ff')
x  = np.linspace(0, 10.1, 100)
maxagg = plt.plot(x ,maxaggstation1, label = 'Max Aggregation', color = '#8c8184')
x  = np.linspace(0, 10.1, 100)
rfragg = plt.plot(x ,rfraggstation1, label = 'RFR Model', color = '#E2583E')

plt.ylabel('Goodput [Mb]')
plt.xlabel('Time [s]')
plt.legend(loc='upper left')
plt.yticks(np.linspace(0, 0.5, 6))
plt.ylim(ymax = 0.5, ymin = 0)
plt.xlim(xmax = 10, xmin = 0)
plt.xticks(np.linspace(1, 10, 10))
plt.savefig('Station1.pdf',bbox_inches='tight', transparent="True", pad_inches=0)
plt.show()

axes= plt.axes()
axes.grid()
axes.set_axisbelow(True)

with open('Goodputaeach2stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggstation2.append(float(row[0]))
csvfile.close()

with open('Goodputaeach2stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggstation2.append(float(row[0]))
csvfile.close()

with open('Goodputaeach2stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggstation2.append(float(row[0]))
csvfile.close()

x  = np.linspace(0, 10.1, 100)
noagg = plt.plot(x ,noaggstation2, label = 'No Aggregation', color = '#1e3f63ff')
x  = np.linspace(0, 10.1, 100)
maxagg = plt.plot(x ,maxaggstation2, label = 'Max Aggregation', color = '#8c8184')
x  = np.linspace(0, 10.1, 100)
rfragg = plt.plot(x ,rfraggstation2, label = 'RFR Model', color = '#E2583E')

plt.ylabel('Goodput [Mb]')
plt.xlabel('Time [s]')
plt.legend(loc='upper left')
plt.yticks(np.linspace(0, 0.5, 6))
plt.ylim(ymax = 0.5, ymin = 0)
plt.xlim(xmax = 10, xmin = 0)
plt.xticks(np.linspace(1, 10, 10))
plt.savefig('Station2.pdf',bbox_inches='tight', transparent="True", pad_inches=0)
plt.show()


axes= plt.axes()
axes.grid()
axes.set_axisbelow(True)
with open('Goodputaeach3stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggstation3.append(float(row[0]))
csvfile.close()

with open('Goodputaeach3stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggstation3.append(float(row[0]))
csvfile.close()

with open('Goodputaeach3stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggstation3.append(float(row[0]))
csvfile.close()

x  = np.linspace(0, 10.1, 100)
noagg = plt.plot(x ,noaggstation3, label = 'No Aggregation', color = '#1e3f63ff')
x  = np.linspace(0, 10.1, 100)
maxagg = plt.plot(x ,maxaggstation3, label = 'Max Aggregation', color = '#8c8184')
x  = np.linspace(0, 10.1, 100)
rfragg = plt.plot(x ,rfraggstation3, label = 'RFR Model', color = '#E2583E')

plt.ylabel('Goodput [Mb]')
plt.xlabel('Time [s]')
plt.legend(loc='upper left')
plt.yticks(np.linspace(0, 0.5, 6))
plt.ylim(ymax = 0.5, ymin = 0)
plt.xlim(xmax = 10, xmin = 0)
plt.xticks(np.linspace(1, 10, 10))
plt.savefig('Station3.pdf',bbox_inches='tight', transparent="True", pad_inches=0)
plt.show()




axes= plt.axes()
axes.grid()
axes.set_axisbelow(True)
with open('Goodputaeach4stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggstation4.append(float(row[0]))
csvfile.close()

with open('Goodputaeach4stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggstation4.append(float(row[0]))
csvfile.close()

with open('Goodputaeach4stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggstation4.append(float(row[0]))
csvfile.close()
x  = np.linspace(0, 10.1, 100)
noagg = plt.plot(x ,noaggstation4, label = 'No Aggregation', color = '#1e3f63ff')
x  = np.linspace(0, 10.1, 100)
maxagg = plt.plot(x ,maxaggstation4, label = 'Max Aggregation', color = '#8c8184')
x  = np.linspace(0, 10.1, 100)
rfragg = plt.plot(x ,rfraggstation4, label = 'RFR Model', color = '#E2583E')

plt.ylabel('Goodput [Mb]')
plt.xlabel('Time [s]')
plt.legend(loc='upper left')
plt.yticks(np.linspace(0, 0.5, 6))
plt.ylim(ymax = 0.5, ymin = 0)
plt.xlim(xmax = 10, xmin = 0)
plt.xticks(np.linspace(1, 10, 10))
plt.savefig('Station4.pdf',bbox_inches='tight', transparent="True", pad_inches=0)
plt.show()




axes= plt.axes()
axes.grid()
axes.set_axisbelow(True)
with open('Goodputaeach5stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggstation5.append(float(row[0]))
csvfile.close()

with open('Goodputaeach5stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggstation5.append(float(row[0]))
csvfile.close()

with open('Goodputaeach5stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggstation5.append(float(row[0]))
csvfile.close()

x = np.linspace(0, 10.1, 100)
noagg = plt.plot(x,noaggstation5, label = 'No Aggregation', color = '#1e3f63ff')
x = np.linspace(0, 10.1, 100)
maxagg = plt.plot(x,maxaggstation5, label = 'Max Aggregation', color = '#8c8184')
x = np.linspace(0, 10.1, 100)
rfragg = plt.plot(x,rfraggstation5, label = 'RFR Model', color = '#E2583E')

plt.ylabel('Goodput [Mb]')
plt.xlabel('Time [s]')
plt.legend(loc='upper left')
plt.yticks(np.linspace(0, 0.5, 6))
plt.ylim(ymax = 0.5, ymin = 0)
plt.xlim(xmax = 10, xmin = 0)
plt.xticks(np.linspace(1, 10, 10))
plt.savefig('Station5.pdf',bbox_inches='tight', transparent="True", pad_inches=0)
plt.show()



with open('Goodputaeach1stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggtotal.append(float(row[0]))
csvfile.close()
with open('Goodputaeach2stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach3stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach4stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach5stationnoaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        noaggtotal[i]+=(float(row[0]))
csvfile.close()



with open('Goodputaeach1stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggtotal.append(float(row[0]))
csvfile.close()
with open('Goodputaeach2stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach3stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach4stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach5stationmaxaggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        maxaggtotal[i]+=(float(row[0]))
csvfile.close()




with open('Goodputaeach1stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggtotal.append(float(row[0]))
csvfile.close()
with open('Goodputaeach2stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach3stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach4stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggtotal[i]+=(float(row[0]))
csvfile.close()
with open('Goodputaeach5stationrfraggregation.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        rfraggtotal[i]+=(float(row[0]))
csvfile.close()

axes= plt.axes()
axes.grid()
axes.set_axisbelow(True)
x = np.linspace(0, 10.1, 100)
noagg = plt.plot(x,noaggtotal, label = 'No Aggregation', color = '#1e3f63ff')
x = np.linspace(0, 10.1, 100)
maxagg = plt.plot(x,maxaggtotal, label = 'Max Aggregation', color = '#8c8184')
x = np.linspace(0, 10.1, 100)
rfragg = plt.plot(x,rfraggtotal, label = 'RFR Model', color = '#E2583E')

plt.ylabel('Goodput [Mb]')
plt.xlabel('Time [s]')
plt.legend(loc='upper left')
plt.yticks(np.linspace(0, 2, 5))
plt.ylim(ymax = 2, ymin = 0)
plt.xlim(xmax = 10, xmin = 0)
plt.xticks(np.linspace(1, 10, 10))
plt.savefig('Allaggregate.pdf',bbox_inches='tight', transparent="True", pad_inches=0)
plt.show()
