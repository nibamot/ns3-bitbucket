#!/usr/bin/env python3
import csv
import matplotlib.pyplot as pyplot
import seaborn as sns
import numpy as np
from scipy.interpolate import interp1d
from matplotlib.backends.backend_pdf import PdfPages

NUM_COLORS = 10
LINE_STYLES = ['solid', 'dashed', 'dashdot', 'dotted']
NUM_STYLES = len(LINE_STYLES)

sns.reset_orig()
clrs = sns.color_palette('husl', n_colors=NUM_COLORS)
# MCS 0 6.5

a = [0, 450, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 7935]
b = [0, 3.70377, 3.97087, 3.90873, 3.76883, 3.56764, 3.38619, 3.20063, 2.97781, 2.77559, 2.57681, 2.42764, 2.25821, 2.09753, 2.00346, 1.88828, 1.88828]
a_smooth = np.array(a)
b_smooth = np.array(b)
aa_smooth = np.linspace(a_smooth.min(), a_smooth.max(), 500)
spl = interp1d(a_smooth, b_smooth, kind='quadratic')
bb_smooth = spl(aa_smooth)

c = [0, 450, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 7935]
d = [0, 5.55719, 6.01501, 5.68182, 5.0786, 4.48328, 3.958, 3.44799, 3.04613, 2.70504, 2.42849, 2.18388, 1.90784, 1.66443, 1.49603, 1.28283, 1.28283]
c_smooth = np.array(c)
d_smooth = np.array(d)
cc_smooth = np.linspace(c_smooth.min(), c_smooth.max(), 500)
spl = interp1d(c_smooth, d_smooth, kind='quadratic')
dd_smooth = spl(cc_smooth)

e = [0, 450, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 5500, 6000, 6500, 7000, 7500, 7935]
f = [0, 10.2923, 12.8983, 12.3025, 11.18, 9.53373, 7.96303, 6.72089, 5.71564, 5.02401, 4.32209, 3.60214, 3.19393, 2.79259, 2.41596, 2.11195, 2.11195]
e_smooth = np.array(e)
f_smooth = np.array(f)
ee_smooth = np.linspace(e_smooth.min(), e_smooth.max(), 500)
spl = interp1d(e_smooth, f_smooth, kind='quadratic')
ff_smooth = spl(ee_smooth)



o1=[0]
pyplot.rcParams.update({'font.size': 35})
o2=[14]

# matplotlib plot
pyplot.plot(o1,o2)
pyplot.plot(aa_smooth,bb_smooth,label='MCS 0, 5Mbps, Distance of 95 m', color='b', linestyle=LINE_STYLES[0], linewidth=2.5)
pyplot.plot(cc_smooth,dd_smooth,label='MCS 1, 10 Mbps, Distance of 76.5 m', color='b', linestyle=LINE_STYLES[1], linewidth=2.5)
pyplot.plot(ee_smooth,ff_smooth,label='MCS 7, 35 Mbps, Distance of 21.5 m', color='b', linestyle=LINE_STYLES[3], linewidth=2.5)
#pyplot.gca().spines['bottom'].set_position(('data',0))
#pyplot.gca().spines['bottom'].set_smart_bounds(True)
#pyplot.gca().spines['left'].set_position(('data',0))
#pyplot.gca().spines['left'].set_smart_bounds(True)
#pyplot.gca().spines['top'].set_smart_bounds(True)
#pyplot.gca().spines['right'].set_position(('data',7935))
#pyplot.gca().spines['right'].set_smart_bounds(True)
pyplot.yticks([2, 4, 6, 8, 10, 12, 14])
pyplot.xticks([0, 1000, 2000, 3000, 4000,5000, 6000, 7000, 8000])
pyplot.xlabel('Frame length [bytes]')
pyplot.ylabel('Goodput [Mb/s]')
pyplot.legend()#loc = 'upper left', bbox_to_anchor=(0.59, 1.0)
pyplot.show()
