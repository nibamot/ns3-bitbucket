#!/usr/bin/env python3
import matplotlib.pyplot as pyplot

# MCS 0 256
a = [256, 512, 1024, 1470]
b = [0.770252, 0.841647, 0.879687, 0.87979]

# MCS 0 512
c = [1000, 2000, 3000, 5000, 6000, 7000, 7935]
d = [0.695072, 0.761358, 0.763702, 0.765264, 0.765865, 0.765625, 0.765745]

# MCS 0 1024
e = [1000, 2000, 3000, 5000, 6000, 7000, 7935]
f = [0.761298, 0.761298, 0.765505, 0.768029, 0.768149, 0.76851, 0.768389]


# MCS 0 1470
g = [1000, 2000, 3000, 5000, 6000, 7000, 7935]
h = [0.76505, 0.76505, 0.76505, 0.769018, 0.769018, 0.769018, 0.769018]

o1=[800]
o2=[0.65]
# matplotlib plot
pyplot.plot(o1,o2)
pyplot.plot(a,b,label='Payload=256, MCS 0',color='r')
#pyplot.plot(c,d,label='Payload=512, MCS 0',color='b')
#pyplot.plot(e,f,label='Payload=1024, MCS 0',color='g')
#pyplot.plot(g,h,label='Payload=1470, MCS 0',color='y')
pyplot.xlabel('Payload Size')
pyplot.ylabel('Channel Util')
pyplot.title('Payload size vs Channel Util for  MCS 0, 1 Stations, 6 Mbps/Station')
pyplot.legend()
pyplot.show()
