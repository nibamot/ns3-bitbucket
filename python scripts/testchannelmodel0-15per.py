#!/usr/bin/env python3
import csv
import matplotlib.pyplot as pyplot

# MCS 0 6.5
a = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
bstring = []
b=[]
abr = 5
with open('Output-Training-A1payload1470MCS0.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            bstring.append(row[16])

csvfile.close()
b = list (map(float, bstring))



# MCS 1 13
c = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
dstring = []
d=[]
cbr = 9
with open('Output-Training-A1payload1470MCS1.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            dstring.append(row[16])

csvfile.close()
d = list (map(float, dstring))



# MCS 2
e = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
fstring = []
f=[]
ebr = 16.6
with open('Output-Training-A1payload1470MCS2.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            fstring.append(row[16])

csvfile.close()
f = list (map(float, fstring))



# MCS 3
g = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
hstring = []
h=[]
gbr = 22
with open('Output-Training-A1payload1470MCS3.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            hstring.append(row[16])

csvfile.close()
h = list (map(float, hstring))

# MCS 4
i1 = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
jstring = []
j=[]
i1br = 31.4
with open('Output-Training-A1payload1470MCS4.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            jstring.append(row[16])

csvfile.close()
j = list (map(float, jstring))


# MCS 5
k = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
lstring = []
l=[]
kbr = 41
with open('Output-Training-A1payload1470MCS5.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            lstring.append(row[16])

csvfile.close()
l = list (map(float, lstring))


# MCS 6
m = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
nstring = []
n=[]
mbr = 42
with open('Output-Training-A1payload1470MCS6.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            nstring.append(row[16])

csvfile.close()
n = list (map(float, nstring))



# MCS 7
o = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
pstring = []
p=[]
obr = 43.8
with open('Output-Training-A1payload1470MCS7.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            pstring.append(row[16])

csvfile.close()
p = list (map(float, pstring))




# MCS 8
q = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
rstring = []
r=[]
qbr = 11.3
with open('Output-Training-A1payload1470MCS8.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            rstring.append(row[16])

csvfile.close()
r = list (map(float, rstring))



# MCS 9
s = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
tstring = []
t=[]
sbr = 22
with open('Output-Training-A1payload1470MCS9.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            tstring.append(row[16])

csvfile.close()
t = list (map(float, tstring))



# MCS 10
u = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
vstring = []
v=[]
ubr = 31.4
with open('Output-Training-A1payload1470MCS10.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            vstring.append(row[16])

csvfile.close()
v = list (map(float, vstring))




# MCS 11
w = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
xstring = []
x1=[]
wbr = 41
with open('Output-Training-A1payload1470MCS11.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            xstring.append(row[16])

csvfile.close()
x1 = list (map(float, xstring))



# MCS 12
y = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
zstring = []
z=[]
ybr = 56.8
with open('Output-Training-A1payload1470MCS12.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            zstring.append(row[16])

csvfile.close()
z = list (map(float, zstring))


# MCS 13
a1 = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
a2string = []
a2=[]
a1br = 70
with open('Output-Training-A1payload1470MCS13.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            a2string.append(row[16])

csvfile.close()
a2 = list (map(float, a2string))




# MCS 14
b1 = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
b2string = []
b2=[]
b1br = 76.8
with open('Output-Training-A1payload1470MCS14.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            b2string.append(row[16])

csvfile.close()
b2 = list (map(float, b2string))



# MCS 15
c1 = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160]
c2string = []
c2=[]
c1br = 84
with open('Output-Training-A1payload1470MCS15.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            c2string.append(row[16])

csvfile.close()
c2 = list (map(float, c2string))




o1=[0]
o2=[0]

# matplotlib plot
pyplot.plot(o1,o2)
pyplot.plot(a,b,label='MCS 0, 5.76Mbps')
pyplot.plot(c,d,label='MCS 1, 11Mbps')
pyplot.plot(e,f,label='MCS 2, 16Mbps')
pyplot.plot(g,h,label='MCS 3, 22Mbps')
pyplot.plot(i1,j,label='MCS 4, 31.4Mbps')
pyplot.plot(k,l,label='MCS 5, 41Mbps')
pyplot.plot(m,n,label='MCS 6, 42Mbps')
pyplot.plot(o,p,label='MCS 7, 43.8Mbps')
pyplot.plot(q,r,label='MCS 8, 11.3Mbps')
pyplot.plot(s,t,label='MCS 9, 22Mbps')
pyplot.plot(u,v,label='MCS 10, 31.4Mbps')
pyplot.plot(w,x1,label='MCS 11, 41Mbps')
pyplot.plot(y,z,label='MCS 12, 56.8Mbps')
pyplot.plot(a1,a2,label='MCS 13, 70Mbps')
pyplot.plot(b1,b2,label='MCS 14, 76.8Mbps')
pyplot.plot(c1,c2,label='MCS 15, 84Mbps')
pyplot.xlabel('Distance from AP at (0,0)')
pyplot.ylabel('Packet Loss Ratio')
pyplot.title('Distance in metres vs Packet Loss ratio for  Payload 1470, 1 Station  ')
pyplot.legend()

pyplot.show()
