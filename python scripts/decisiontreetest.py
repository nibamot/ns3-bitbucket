#!/usr/bin/env python3
#after 420 lines change aggregation size then mcs
import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import tree
from pylab import rcParams

rcParams["figure.figsize"] =20,12
Y=[]
X = np.genfromtxt('avgkpi200.csv',delimiter=',',dtype=float, usecols=(0,1,2,3,4))
print(X)
#X = pd.[[2.7082316,0.118782075,78.37639,0.0182741545,576000],[3.50509665,0.153732301,72.01388,0.023651151,576000]]
with open('avgkpi200.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        Y.append(row[5])
print(Y)

clf = tree.DecisionTreeClassifier()#DecisionTreeRegressor#DecisionTreeClassifier
clf = clf.fit(X,Y)
print(clf.predict([[2.364541,0.10372545,23.11737,0.09957745,576000]]))
print(clf.predict_proba([[2.364941,0.10372545,81.11737,0.015957745,576000]]))

tree.plot_tree(clf.fit(X,Y))
plt.show()
