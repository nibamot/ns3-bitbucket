#!/usr/bin/env python3
#after 420 lines change aggregation size then mcs

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import csv
from matplotlib import cm


fig = plt.figure()
ax = fig.gca(projection='3d')

z=[]
avg = 0
p = 0
with open('DupOutput-Training-A200avg.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)

    for i, row in enumerate(csvreader):
        if i % 21 == 0 and i!=0:
            avg += float(row[15])
            p+=1
            if p == 5:
                avg = float(avg/5)
                z.append(avg)
                avg = 0
                p = 0
            #print(z)
            #break
x = [1500.0, 3200.0, 4596.0, 7935.0]

i=0
xs= []
while i < 16:
    xs.insert(i, 1500.0)
    i+=1

while i < 32:
    xs.insert(i, 3200.0)
    i+=1

while i < 48:
    xs.insert(i, 4596.0)
    i+=1

while i < 64:
    xs.insert(i, 7935.0)
    i+=1

y = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0,
     0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0,
     0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0,
     0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0]
#np.array(x, dtype=np.uint32)
#np.array(y, dtype=np.uint32)
#np.array(z, dtype=np.float64)
#z = list(np.around(np.array(z),0))
print(len(z))
X = np.reshape(xs, (16,4))
Y = np.reshape(y, (16,4))
zs = np.asarray(z)
Z = zs.reshape(X.shape)
surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)
fig.colorbar(surf, shrink=0.5, aspect=5)
"""l = 0
for j in x:
    for k in y:

        if l <  len(z):
            ax.scatter(j, k, z[l])
            l+=1
"""



ax.set_xlabel('Aggregation size')
ax.set_ylabel('MCS ')
ax.set_zlabel('Packet Loss ratio')

plt.show()
