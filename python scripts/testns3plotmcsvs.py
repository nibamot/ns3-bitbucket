#!/usr/bin/env python3
import csv
import matplotlib.pyplot as pyplot

# MCS 0 6.5
a = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120]
bstring = []
b=[]

with open('Output-Training-A1payload1470MCS6.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            bstring.append(row[16])

csvfile.close()
bnum = list (map(float, bstring))
b = [((100-x)) for x in bnum]


# MCS 1 13
c = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120]
dstring = []
d=[]
with open('Output-Training-A1payload1470MCS1.csv', 'r') as csvfile:
    csvreader = csv.reader(csvfile)
    for i, row in enumerate(csvreader):
        if i==1 or i%3==1:
            dstring.append(row[16])

csvfile.close()
dnum = list (map(float, dstring))
d = [((100-x)) for x in dnum]





o1=[0]
o2=[0]

# matplotlib plot
pyplot.plot(o1,o2)
pyplot.plot(a,b,label='MCS 6, 54Mbps')
pyplot.plot(c,d,label='MCS 1, 12Mbps')
pyplot.xlabel('Distance from AP at (0,0)')
pyplot.ylabel(' % Probability of delivery')
pyplot.title('Distance in metres vs Probability of delivery for  Payload 1470, 1 Station  ')
pyplot.legend()

pyplot.show()
